# PDF_MAKER

## Programming Language
- Kotlin
- JAVA

## Design Pattern
- MVVM
- Singleton

## Used Architectural Component
- Room DB
- ViewModel
- LiveData
- DataBinding
- Navigation Component

## Features
- Light/dark theme mode
- PDF View
- Create PDF from Image
- Add Signature on PDF
- Store your own Signature
- Custom Text Editor
- Custom PDF from Text Editor
- Scan Any document
- Create custom folder and store your updated PDF 

