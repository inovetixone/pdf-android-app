package com.Inovetixone.pdf.ui.text_editor;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.Inovetixone.pdf.MainActivity;
import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.databinding.ActivityEditorPreviewBinding;
import com.Inovetixone.pdf.helper.Document.PDSSaveAsPDFAsyncTask;
import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.webviewtopdf.PdfView;

import java.io.File;

public class EditorPreviewActivity extends AppCompatActivity {

    ActivityEditorPreviewBinding binding;
    String editorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditorPreviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportActionBar().setTitle("Preview");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


         editorText = getIntent().getStringExtra("text");
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        binding.webView.loadDataWithBaseURL("", editorText, mimeType, encoding, "");


        binding.button.setOnClickListener(v -> {
            savePDFDocument();
        });

    }

    public void savePDFDocument() {
        final Dialog dialog = new Dialog(EditorPreviewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View alertView = getLayoutInflater().inflate(R.layout.file_alert_dialog, null);
        final EditText edittext = alertView.findViewById(R.id.editText2);
        dialog.setContentView(alertView);
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        (dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = edittext.getText().toString();
                if (edittext.length() == 0) {
                    Toast.makeText(EditorPreviewActivity.this, "File name should not be empty", Toast.LENGTH_LONG).show();
                } else {
                    downloadData(fileName);
                }
            }
        });
    }


    private void downloadData(String fileName) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
        File myDir = new File(root + "/MyPDF");

        final ProgressDialog progressDialog = new ProgressDialog(EditorPreviewActivity.this);
        progressDialog.setMessage("Please wait");

        PdfView.createWebPrintJob(EditorPreviewActivity.this, binding.webView, myDir, fileName, new PdfView.Callback() {

            @Override
            public void success(String path) {
                Toast.makeText(EditorPreviewActivity.this, "Successfully PDF Created", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(EditorPreviewActivity.this,MainActivity.class));
                finish();
            }

            @Override
            public void failure() {
                progressDialog.dismiss();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
         if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


}