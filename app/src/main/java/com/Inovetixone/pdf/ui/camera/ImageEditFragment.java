package com.Inovetixone.pdf.ui.camera;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.Inovetixone.pdf.MainActivity;
import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.databinding.FragmentImageEditBinding;
import com.Inovetixone.pdf.helper.Document.PDSSaveAsPDFAsyncTask;
import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.Inovetixone.pdf.utils.Constants;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageEditFragment extends Fragment {

    FragmentImageEditBinding binding;
    String imageUri;
    Bitmap bitmap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        binding = FragmentImageEditBinding.inflate(getLayoutInflater());



        return binding.getRoot();
    }

    public void createPdf(String fileName) throws DocumentException, IOException {
        try {
            Document document = new Document();


            String directoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name)+ "/MyPDF/";
            PdfWriter.getInstance(document, new FileOutputStream(directoryPath + fileName+".pdf")); //  Change pdf's name.

            document.open();

            Image image = Image.getInstance(imageUri);  // Change image's name and extension.
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / image.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
            image.scalePercent(scaler);
            image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            image.setAbsolutePosition((document.getPageSize().getWidth() - image.getScaledWidth()) / 2.0f,
                    (document.getPageSize().getHeight() - image.getScaledHeight()) / 2.0f);



            document.add(image);
            document.close();
            Toast.makeText(getActivity(), "Successfully PDF Created", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
        }catch (Exception e){}

    }
    public void savePDFDocument() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View alertView = getLayoutInflater().inflate(R.layout.file_alert_dialog, null);
        final EditText edittext = alertView.findViewById(R.id.editText2);
        dialog.setContentView(alertView);
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        (dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = edittext.getText().toString();
                if (fileName.length() == 0) {
                    Toast.makeText(getActivity(), "File name should not be empty", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        createPdf(fileName);
                    } catch (DocumentException | IOException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            }
        });
    }

}