package com.Inovetixone.pdf.ui.pdf_view;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.helper.DatabaseHandler;
import com.Inovetixone.pdf.helper.Document.PDSElementViewer;
import com.Inovetixone.pdf.helper.Document.PDSPageViewer;
import com.Inovetixone.pdf.helper.Document.PDSSaveAsPDFAsyncTask;
import com.Inovetixone.pdf.helper.Document.PDSViewPager;
import com.Inovetixone.pdf.helper.PDF.PDSPDFDocument;
import com.Inovetixone.pdf.helper.PDSModel.PDSElement;
import com.Inovetixone.pdf.helper.Signature.SignatureActivity;
import com.Inovetixone.pdf.helper.Signature.SignatureUtils;
import com.Inovetixone.pdf.ui.adapter.PDSPageAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.security.KeyStore;
import java.util.ArrayList;

public class PdfViewActivity extends AppCompatActivity {

    private static final int READ_REQUEST_CODE = 42;
    private static final int SIGNATURE_Request_CODE = 43;
    private static final int IMAGE_REQUEST_CODE = 45;
    Uri pdfData = null;
    private PDSViewPager mViewPager;
    PDSPageAdapter imageAdapter;
    private RecyclerView mRecyclerView;
    private boolean mFirstTap = true;
    private int mVisibleWindowHt = 0;
    private PDSPDFDocument mDocument = null;
    private Menu mMenu = null;
    private final UIElementsHandler mUIElemsHandler = new UIElementsHandler(this);
    AlertDialog signatureOptionDialog;
    public KeyStore keyStore = null;
    public String alises = null;
    public boolean isSigned = false;
    public ProgressBar savingProgress;
    private RelativeLayout pdfViewLayout;
    private FloatingActionButton floatingActionButton;
    public String mdigitalIDPassword = null;
    private Menu mmenu = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);

        mViewPager = findViewById(R.id.viewpager);
        savingProgress = findViewById(R.id.savingProgress);
        pdfViewLayout = findViewById(R.id.docviewer);
        floatingActionButton = findViewById(R.id.signatureBtn);



        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String message = intent.getStringExtra("ActivityAction");
        if (message.equals("FileSearch")) {
            performFileSearch();
        } else if (message.equals("PDFOpen")) {
            ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra("PDFOpen");
            Log.wtf("pdfURL",imageUris.get(0).getEncodedPath());


            if (imageUris != null) {
                for (int i = 0; i < imageUris.size(); i++) {
                    Uri imageUri = imageUris.get(i);
                    OpenPDFViewer((imageUri));
                }
            }
        }else if (message.equals("pdf_sign")){
            ab.setTitle("Add Signature");
            floatingActionButton.setVisibility(View.VISIBLE);
            performFileSearch();
        }


        floatingActionButton.setOnClickListener(v -> {
            showSignatureTypeDialog();
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if (requestCode == READ_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (result != null) {
                    pdfData = result.getData();
                    OpenPDFViewer(pdfData);
                }
            } else {
                finish();
            }
        }
        if (requestCode == SIGNATURE_Request_CODE && resultCode == Activity.RESULT_OK) {
            String returnValue = result.getStringExtra("FileName");
            File fi = new File(returnValue);
            this.addElement(PDSElement.PDSElementType.PDSElementTypeSignature, fi, (float) SignatureUtils.getSignatureWidth((int) getResources().getDimension(R.dimen.sign_field_default_height), fi, getApplicationContext()), getResources().getDimension(R.dimen.sign_field_default_height));

        }

        if (requestCode == IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (result != null) {
                Uri imageData = result.getData();
                Bitmap bitmap = null;
                try {
                    InputStream input = this.getContentResolver().openInputStream(imageData);
                    bitmap = BitmapFactory.decodeStream(input);
                    input.close();
                    if (bitmap != null)
                        this.addElement(PDSElement.PDSElementType.PDSElementTypeImage, bitmap, getResources().getDimension(R.dimen.sign_field_default_height), getResources().getDimension(R.dimen.sign_field_default_height));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        this.mmenu = menu;
        MenuItem signItem = mmenu.findItem(R.id.action_save);
        signItem.getIcon().setAlpha(130);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_save) {
            savePDFDocument();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (isSigned) {
            final AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle("Save Document")
                    .setMessage("Want to save your changes to PDF document?")

                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            savePDFDocument();
                        }
                    })
                    .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        } else {
            finish();
        }
    }

    private void OpenPDFViewer(Uri pdfData) {
        try {
            PDSPDFDocument document = new PDSPDFDocument(this, pdfData);
            document.open();
            this.mDocument = document;
            imageAdapter = new PDSPageAdapter(getSupportFragmentManager(), document);
            updatePageNumber(1);
            mViewPager.setAdapter(imageAdapter);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(PdfViewActivity.this, "Cannot open PDF, either PDF is corrupted or password protected", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/jpeg");
        String[] mimetypes = {"application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private int computeVisibleWindowHtForNonFullScreenMode() {
        return findViewById(R.id.docviewer).getHeight();
    }


    public boolean isFirstTap() {
        return this.mFirstTap;
    }

    public void setFirstTap(boolean z) {
        this.mFirstTap = z;
    }

    public int getVisibleWindowHeight() {
        if (this.mVisibleWindowHt == 0) {
            this.mVisibleWindowHt = computeVisibleWindowHtForNonFullScreenMode();
        }
        return this.mVisibleWindowHt;
    }

    public PDSPDFDocument getDocument() {
        return this.mDocument;
    }



    public void addElement(PDSElement.PDSElementType fASElementType, File file, float f, float f2) {
        View focusedChild = this.mViewPager.getFocusedChild();
        if (focusedChild != null) {
            PDSPageViewer fASPageViewer = (PDSPageViewer) ((ViewGroup) focusedChild).getChildAt(0);
            if (fASPageViewer != null) {
                RectF visibleRect = fASPageViewer.getVisibleRect();
                float width = (visibleRect.left + (visibleRect.width() / 2.0f)) - (f / 2.0f);
                float height = (visibleRect.top + (visibleRect.height() / 2.0f)) - (f2 / 2.0f);
                PDSElementViewer lastFocusedElementViewer = fASPageViewer.getLastFocusedElementViewer();

                PDSElement.PDSElementType fASElementType2 = fASElementType;
                final PDSElement element = fASPageViewer.createElement(fASElementType2, file, width, height, f, f2);

                invokeMenuButton(true);
            }

        }
    }

    public void invokeMenuButton(boolean disableButtonFlag) {
        MenuItem saveItem = mmenu.findItem(R.id.action_save);
        saveItem.setEnabled(disableButtonFlag);
        isSigned = disableButtonFlag;
        if (disableButtonFlag) {
            saveItem.getIcon().setAlpha(255);
        } else {
            saveItem.getIcon().setAlpha(130);

        }
    }


    public void addElement(PDSElement.PDSElementType fASElementType, Bitmap bitmap, float f, float f2) {
        View focusedChild = this.mViewPager.getFocusedChild();
        if (focusedChild != null && bitmap != null) {
            PDSPageViewer fASPageViewer = (PDSPageViewer) ((ViewGroup) focusedChild).getChildAt(0);
            if (fASPageViewer != null) {
                RectF visibleRect = fASPageViewer.getVisibleRect();
                float width = (visibleRect.left + (visibleRect.width() / 2.0f)) - (f / 2.0f);
                float height = (visibleRect.top + (visibleRect.height() / 2.0f)) - (f2 / 2.0f);
                PDSElementViewer lastFocusedElementViewer = fASPageViewer.getLastFocusedElementViewer();

                PDSElement.PDSElementType fASElementType2 = fASElementType;
                final PDSElement element = fASPageViewer.createElement(fASElementType2, bitmap, width, height, f, f2);
            }
        }
    }

    public void updatePageNumber(int i) {
        TextView textView = (TextView) findViewById(R.id.pageNumberTxt);
        findViewById(R.id.pageNumberOverlay).setVisibility(View.VISIBLE);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append("/");
        stringBuilder.append(this.mDocument.getNumPages());
        textView.setText(stringBuilder.toString());
        resetTimerHandlerForPageNumber(1000);
    }

    private void resetTimerHandlerForPageNumber(int i) {
        this.mUIElemsHandler.removeMessages(1);
        Message message = new Message();
        message.what = 1;
        this.mUIElemsHandler.sendMessageDelayed(message, (long) i);
    }

    private void fadePageNumberOverlay() {
//        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
//        View findViewById = findViewById(R.id.pageNumberOverlay);
//        if (findViewById.getVisibility() == View.VISIBLE) {
//            findViewById.startAnimation(loadAnimation);
//            findViewById.setVisibility(View.INVISIBLE);
//        }
    }

    private static class UIElementsHandler extends Handler {
        private final WeakReference<PdfViewActivity> mActivity;

        public UIElementsHandler(PdfViewActivity fASDocumentViewer) {
            this.mActivity = new WeakReference(fASDocumentViewer);
        }

        public void handleMessage(Message message) {
            PdfViewActivity fASDocumentViewer = this.mActivity.get();
            if (fASDocumentViewer != null && message.what == 1) {
                fASDocumentViewer.fadePageNumberOverlay();
            }
            super.handleMessage(message);
        }
    }


    public void runPostExecution() {
        savingProgress.setVisibility(View.INVISIBLE);
        makeResult();

    }

    public void makeResult() {
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        finish();
    }

    public void savePDFDocument() {
        final Dialog dialog = new Dialog(PdfViewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View alertView = getLayoutInflater().inflate(R.layout.file_alert_dialog, null);
        final EditText edittext = alertView.findViewById(R.id.editText2);
        dialog.setContentView(alertView);
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        (dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = edittext.getText().toString();
                if (fileName.length() == 0) {
                    Toast.makeText(PdfViewActivity.this, "File name should not be empty", Toast.LENGTH_LONG).show();
                } else {
                    PDSSaveAsPDFAsyncTask task = new PDSSaveAsPDFAsyncTask(PdfViewActivity.this, fileName + ".pdf");
                    task.execute(new Void[0]);
                    dialog.dismiss();
                }
            }
        });
    }


    private void showSignatureTypeDialog(){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.optiondialog, null);
        dialogBuilder.setView(dialogView);

        Button signature = dialogView.findViewById(R.id.fromCollection);

        signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignatureActivity.class);
                startActivityForResult(intent, SIGNATURE_Request_CODE);
                signatureOptionDialog.dismiss();
            }
        });


        Button image = dialogView.findViewById(R.id.fromImage);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/jpeg");
                String[] mimetypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                startActivityForResult(intent, IMAGE_REQUEST_CODE);
                signatureOptionDialog.dismiss();
            }
        });

        signatureOptionDialog = dialogBuilder.create();
        signatureOptionDialog.show();


    }

}