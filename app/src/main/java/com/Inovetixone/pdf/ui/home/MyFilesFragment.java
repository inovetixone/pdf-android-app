package com.Inovetixone.pdf.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.databinding.FragmentMyFilesBinding;
import com.Inovetixone.pdf.helper.DatabaseHandler;
import com.Inovetixone.pdf.ui.adapter.MainRecycleViewAdapter;
import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MyFilesFragment extends Fragment {

    FragmentMyFilesBinding binding;
    List<File> items = null;
    private MainRecycleViewAdapter mAdapter;
    private Handler mHandler = new Handler();
    private BottomSheetDialog mBottomSheetDialog;
    private DatabaseHandler databaseHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMyFilesBinding.inflate(inflater);
        databaseHandler = DatabaseHandler.getInstance(requireActivity());

        binding.mainRecycleView.setEmptyView(binding.toDoEmptyView);
        binding.mainRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.mainRecycleView.setHasFixedSize(true);

        binding.toolbar.title.setVisibility(View.VISIBLE);
        binding.toolbar.title.setText("MY NOTES");

        binding.toolbar.backbtn.setOnClickListener(view -> {
            getActivity().onBackPressed();
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        CreateDataSource();

    }

    private void CreateDataSource() {
        if (binding.mainRecycleView != null) {
            items = new ArrayList<File>();
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
            File myDir = new File(root + "/MyPDF");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            File[] files = myDir.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    long result = file2.lastModified() - file1.lastModified();
                    if (result < 0) {
                        return -1;
                    } else if (result > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });

            for (int i = 0; i < files.length; i++) {
                items.add(files[i]);
            }

            //set data and list adapter
            mAdapter = new MainRecycleViewAdapter(getActivity(), items);
            mAdapter.setOnItemClickListener(new MainRecycleViewAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, File value, int position) {
                    showBottomSheetDialog(value);
                }

                @Override
                public void onItemLongClick(View view, File obj, int pos) {
                    showBottomSheetDialog(obj);
                }

            });

            binding.mainRecycleView.setAdapter(mAdapter);
        }
    }


    private void showBottomSheetDialog(final File currentFile) {
        final View view = getLayoutInflater().inflate(R.layout.bottom_sheet, null);

        (view.findViewById(R.id.lyt_email)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = new Intent(Intent.ACTION_SEND);
                target.setType("text/plain");
                target.putExtra(Intent.EXTRA_STREAM, contentUri);
                target.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                startActivity(Intent.createChooser(target, "Send via Email..."));
            }
        });

        (view.findViewById(R.id.lyt_share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = ShareCompat.IntentBuilder.from(getActivity()).setStream(contentUri).getIntent();
                target.setData(contentUri);
                target.setType("application/pdf");
                target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (target.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(target);
                }
            }
        });

        (view.findViewById(R.id.lyt_rename)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomRenameDialog(currentFile);

            }
        });

        (view.findViewById(R.id.lyt_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomDeleteDialog(currentFile);

            }
        });


        (view.findViewById(R.id.lyt_openFile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                databaseHandler.addRecentFile(currentFile.getAbsolutePath());
                Intent target = new Intent(Intent.ACTION_VIEW);
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                ArrayList<Uri> list = new ArrayList<>();
                list.add(contentUri);
                startPdfActivity("PDFOpen", list);



            }
        });
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        mBottomSheetDialog.setContentView(view);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    public void showCustomRenameDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.rename_layout, null);
        builder.setView(view);
        final EditText editText = (EditText) view.findViewById(R.id.renameEditText2);
        editText.setText(currentFile.getName());
        builder.setTitle("Rename");
        builder.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
                File file = new File(root + "/MyPDF", editText.getText().toString());
                currentFile.renameTo(file);
                dialog.dismiss();
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showCustomDeleteDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure want to delete this file?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                databaseHandler.deleteRecentFile(currentFile.getAbsolutePath());
                currentFile.delete();
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startPdfActivity(String message, ArrayList<Uri> imageUris) {
        Intent intent = new Intent(getActivity(), PdfViewActivity.class);
        intent.putExtra("ActivityAction", message);
        intent.putExtra("PDFOpen", imageUris);
        startActivity(intent);
    }

}