package com.Inovetixone.pdf.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.databinding.FragmentSearchBinding;
import com.Inovetixone.pdf.helper.DatabaseHandler;
import com.Inovetixone.pdf.ui.adapter.MainRecycleViewAdapter;
import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.Inovetixone.pdf.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SearchFragment extends Fragment {

    FragmentSearchBinding binding;
    private MainRecycleViewAdapter mAdapter;
    private Handler mHandler = new Handler();
    private BottomSheetDialog mBottomSheetDialog;
    private DatabaseHandler databaseHandler;
    List<File> items = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSearchBinding.inflate(getLayoutInflater());
        databaseHandler = DatabaseHandler.getInstance(requireActivity());

        binding.mainRecycleView.setEmptyView(binding.toDoEmptyView);
        binding.mainRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.mainRecycleView.setHasFixedSize(true);

        // setup search visibility
        binding.toolbar.title.setVisibility(View.VISIBLE);
        binding.toolbar.title.setText("Search");
        binding.toolbar.backbtn.setVisibility(View.VISIBLE);
        binding.toolbar.title.setVisibility(View.GONE);
        binding.toolbar.searchLay.setVisibility(View.VISIBLE);
        binding.toolbar.buttonLayout.setVisibility(View.GONE);
        binding.toolbar.searchView.requestFocus();
        Constants.showKeyboard(requireActivity(), binding.toolbar.searchView);

        binding.toolbar.backbtn.setOnClickListener(v -> {
            if (binding.toolbar.searchLay.getVisibility() == View.VISIBLE) {
                binding.toolbar.searchView.setText("");
                binding.toolbar.buttonLayout.setVisibility(View.VISIBLE);
                binding.toolbar.searchLay.setVisibility(View.GONE);
                binding.toolbar.title.setVisibility(View.VISIBLE);
                Constants.hideSoftKeyboard(requireActivity(), binding.toolbar.searchView);
            } else {
                getActivity().onBackPressed();
            }
        });

        binding.toolbar.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    binding.toolbar.cancelbtn.setVisibility(View.VISIBLE);

                } else {
                    binding.toolbar.cancelbtn.setVisibility(View.GONE);
                }

                searchItem(charSequence.toString());
               // doctorListAdapter.getFilter().filter(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.toolbar.cancelbtn.setOnClickListener(v -> {
            binding.toolbar.searchView.setText("");
        });

        CreateDataSource();

        return binding.getRoot();
    }

    private void searchItem(String searchItem) {

       List<File> localList = new ArrayList<>();
       for (int i=0;i<items.size();i++){
           if (searchItem.contains(items.get(i).getName())){
               localList.add(items.get(i));
           }
       }


        //set data and list adapter
        mAdapter = new MainRecycleViewAdapter(getActivity(), localList);
        mAdapter.setOnItemClickListener(new MainRecycleViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, File value, int position) {
                showBottomSheetDialog(value);
            }

            @Override
            public void onItemLongClick(View view, File obj, int pos) {
                showBottomSheetDialog(obj);
            }

        });

        binding.mainRecycleView.setAdapter(mAdapter);
    }

    private void CreateDataSource() {
        if (binding.mainRecycleView != null) {
            items = new ArrayList<File>();
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
            File myDir = new File(root + "/MyPDF");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            File[] files = myDir.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    long result = file2.lastModified() - file1.lastModified();
                    if (result < 0) {
                        return -1;
                    } else if (result > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });

            for (int i = 0; i < files.length; i++) {
                items.add(files[i]);
            }

        }
    }

    private void showBottomSheetDialog(final File currentFile) {
        final View view = getLayoutInflater().inflate(R.layout.bottom_sheet, null);

        (view.findViewById(R.id.lyt_email)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = new Intent(Intent.ACTION_SEND);
                target.setType("text/plain");
                target.putExtra(Intent.EXTRA_STREAM, contentUri);
                target.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                startActivity(Intent.createChooser(target, "Send via Email..."));
            }
        });

        (view.findViewById(R.id.lyt_share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = ShareCompat.IntentBuilder.from(getActivity()).setStream(contentUri).getIntent();
                target.setData(contentUri);
                target.setType("application/pdf");
                target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (target.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(target);
                }
            }
        });

        (view.findViewById(R.id.lyt_rename)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomRenameDialog(currentFile);

            }
        });

        (view.findViewById(R.id.lyt_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomDeleteDialog(currentFile);

            }
        });


        (view.findViewById(R.id.lyt_openFile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                databaseHandler.addRecentFile(currentFile.getAbsolutePath());
                Intent target = new Intent(Intent.ACTION_VIEW);
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                ArrayList<Uri> list = new ArrayList<>();
                list.add(contentUri);
                startPdfActivity("PDFOpen", list);



            }
        });

        // visibility
        (view.findViewById(R.id.lyt_rename)).setVisibility(View.GONE);
        (view.findViewById(R.id.lyt_delete)).setVisibility(View.GONE);
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        mBottomSheetDialog.setContentView(view);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    public void showCustomRenameDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.rename_layout, null);
        builder.setView(view);
        final EditText editText = (EditText) view.findViewById(R.id.renameEditText2);
        editText.setText(currentFile.getName());
        builder.setTitle("Rename");
        builder.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
                File file = new File(root + "/MyPDF", editText.getText().toString());
                currentFile.renameTo(file);
                dialog.dismiss();
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showCustomDeleteDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure want to delete this file?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                currentFile.delete();
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startPdfActivity(String message, ArrayList<Uri> imageUris) {
        Intent intent = new Intent(getActivity(), PdfViewActivity.class);
        intent.putExtra("ActivityAction", message);
        intent.putExtra("PDFOpen", imageUris);
        startActivity(intent);
    }
}