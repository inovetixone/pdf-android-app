package com.Inovetixone.pdf.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Inovetixone.pdf.R;
import com.Inovetixone.pdf.databinding.FragmentHomeBinding;
import com.Inovetixone.pdf.databinding.FragmentHomeNewBinding;
import com.Inovetixone.pdf.helper.DatabaseHandler;
import com.Inovetixone.pdf.ui.adapter.MainRecycleViewAdapter;
import com.Inovetixone.pdf.ui.adapter.RecentRecycleViewAdapter;

import com.Inovetixone.pdf.scan.AppScanActivity;

import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.Inovetixone.pdf.ui.signature.SignatureListActivity;
import com.Inovetixone.pdf.ui.text_editor.TextEditorActivity;
import com.Inovetixone.pdf.utils.Constants;
import com.Inovetixone.pdf.utils.FilePath;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class HomeFragment extends Fragment {

    FragmentHomeNewBinding binding;
    DatabaseHandler databaseHandler;
    private MainRecycleViewAdapter mAdapter;
    private Handler mHandler = new Handler();
    private BottomSheetDialog mBottomSheetDialog;
    List<File> items = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeNewBinding.inflate(inflater);

        databaseHandler = DatabaseHandler.getInstance(requireActivity());

        binding.pdfViewLayout.setOnClickListener(view -> StartSignatureActivity("FileSearch"));

        binding.signPdfLayout.setOnClickListener(view -> {
            StartSignatureActivity("pdf_sign");
        });

        binding.imageToPdfLayout.setOnClickListener(v -> {
            AppScanActivity.start(requireActivity());
            //Navigation.createNavigateOnClickListener(R.id.cameraFragment).onClick(binding.imageToPdfLayout);
        });

        binding.mySignature.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), SignatureListActivity.class));
        });

        binding.myPdfLayout.setOnClickListener(v -> {
            Navigation.createNavigateOnClickListener(R.id.myFilesFragment).onClick(binding.myPdfLayout);
        });
        binding.searchLayout.setOnClickListener(v -> {
            Navigation.createNavigateOnClickListener(R.id.searchFragment).onClick(binding.searchLayout);
        });

        binding.createPdfLayout.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), TextEditorActivity.class));
        });
//
//

        List<String> filePaths = databaseHandler.getAllRecentFile();
        items = new ArrayList<>();
        for (int i=0;i<filePaths.size();i++){
            try {
                File file = new File(filePaths.get(i));
                items.add(file);

            }catch (Exception e){
                Toast.makeText(requireActivity(), ""+e.getMessage(), Toast.LENGTH_LONG).show();
                Log.wtf("error",e.getMessage());
            }
        }
        Log.wtf("error",""+items.size());
        if (items.size() > 0){
            binding.recycleView.setVisibility(View.VISIBLE);
            binding.toDoEmptyView.setVisibility(View.GONE);
        }else {
            binding.recycleView.setVisibility(View.GONE);
            binding.toDoEmptyView.setVisibility(View.VISIBLE);
        }
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new MainRecycleViewAdapter(getActivity(), items);
        binding.recycleView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new MainRecycleViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, File value, int position) {
                showBottomSheetDialog(value);
            }

            @Override
            public void onItemLongClick(View view, File obj, int pos) {
                showBottomSheetDialog(obj);
            }

        });

        return binding.getRoot();
    }

    public void StartSignatureActivity(String message) {
        Intent intent = new Intent(getActivity(), PdfViewActivity.class);
        intent.putExtra("ActivityAction", message);
        startActivity(intent);
    }

    private void showBottomSheetDialog(final File currentFile) {
        final View view = getLayoutInflater().inflate(R.layout.bottom_sheet, null);

        TextView deleteText = view.findViewById(R.id.deleteTv);
        deleteText.setText("Delete from recent file");
        (view.findViewById(R.id.lyt_email)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = new Intent(Intent.ACTION_SEND);
                target.setType("text/plain");
                target.putExtra(Intent.EXTRA_STREAM, contentUri);
                target.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                startActivity(Intent.createChooser(target, "Send via Email..."));
            }
        });

        (view.findViewById(R.id.lyt_share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                Intent target = ShareCompat.IntentBuilder.from(getActivity()).setStream(contentUri).getIntent();
                target.setData(contentUri);
                target.setType("application/pdf");
                target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (target.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(target);
                }
            }
        });

        (view.findViewById(R.id.lyt_rename)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomRenameDialog(currentFile);

            }
        });

        (view.findViewById(R.id.lyt_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                showCustomDeleteDialog(currentFile);

            }
        });


        (view.findViewById(R.id.lyt_openFile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent target = new Intent(Intent.ACTION_VIEW);
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", currentFile);
                ArrayList<Uri> list = new ArrayList<>();
                list.add(contentUri);
                startPdfActivity("PDFOpen", list);



            }
        });

        // visibility
        (view.findViewById(R.id.lyt_rename)).setVisibility(View.GONE);
//        (view.findViewById(R.id.lyt_delete)).setVisibility(View.GONE);
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        mBottomSheetDialog.setContentView(view);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    public void showCustomRenameDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.rename_layout, null);
        builder.setView(view);
        final EditText editText = (EditText) view.findViewById(R.id.renameEditText2);
        editText.setText(currentFile.getName());
        builder.setTitle("Rename");
        builder.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + getString(R.string.app_name);
                File file = new File(root + "/MyPDF", editText.getText().toString());
                currentFile.renameTo(file);
                dialog.dismiss();
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showCustomDeleteDialog(final File currentFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure want to delete this file?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                databaseHandler.deleteRecentFile(currentFile.getAbsolutePath());
                CreateDataSource();
                mAdapter.notifyItemInserted(items.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startPdfActivity(String message, ArrayList<Uri> imageUris) {
        Intent intent = new Intent(getActivity(), PdfViewActivity.class);
        intent.putExtra("ActivityAction", message);
        intent.putExtra("PDFOpen", imageUris);
        startActivity(intent);
    }

    private void CreateDataSource() {
        items = new ArrayList<>();
        List<String> filePaths = databaseHandler.getAllRecentFile();
        for (int i=0;i<filePaths.size();i++){
            try {
                File file = new File(filePaths.get(i));
                items.add(file);

            }catch (Exception e){
                Toast.makeText(requireActivity(), ""+e.getMessage(), Toast.LENGTH_LONG).show();
                Log.wtf("error",e.getMessage());
            }
        }
        //set data and list adapter
        mAdapter = new MainRecycleViewAdapter(getActivity(), items);
        mAdapter.setOnItemClickListener(new MainRecycleViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, File value, int position) {
                showBottomSheetDialog(value);
            }

            @Override
            public void onItemLongClick(View view, File obj, int pos) {
                showBottomSheetDialog(obj);
            }

        });

        binding.recycleView.setAdapter(mAdapter);
    }


}