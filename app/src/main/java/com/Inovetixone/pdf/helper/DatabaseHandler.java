package com.Inovetixone.pdf.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.Inovetixone.pdf.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Info
    private static final String DATABASE_NAME = Constants.DATABASE_NAME;
    // Table Names
    private static final String TABLE_FILES = "files";
    private static final String TABLE_FOLDERS = "folders";
    private static final String TABLE_RECENT = "recent";
    private static final String[] ALL_TABLES = new String[]{TABLE_FILES, TABLE_FOLDERS, TABLE_RECENT};
    private static final String TAG = "DatabaseHandler";
    private static int DATABASE_VERSION = 3;
    private static DatabaseHandler sInstance;
    Context mContext;

    /**
     * Constructor should be private to prevent direct instantiation.
     * Make a call to the static method "getInstance()" instead.
     */
    private DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public static synchronized DatabaseHandler getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.

        if (sInstance == null) {
            sInstance = new DatabaseHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_FILE_TABLE = "CREATE TABLE " + TABLE_FILES +
                "(" +
                Constants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Constants.FILE_NAME + " TEXT," +
                Constants.FILE_PATH + " TEXT," + Constants.MODIFIED_TIMESTAMP + " TEXT" + ")";

        String CREATE_RECENT_FILE_TABLE = "CREATE TABLE " + TABLE_RECENT +
                "(" +
                Constants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                Constants.FILE_PATH + " TEXT UNIQUE" + ")";

        String CREATE_FOLDER_TABLE = "CREATE TABLE " + TABLE_FOLDERS +
                "(" +
                Constants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Constants.FOLDER_NAME + " TEXT," +
                Constants.FOLDER_COLOR + " TEXT," + Constants.FOLDER_ICON + " TEXT" + ")";


        db.execSQL(CREATE_FILE_TABLE);
        db.execSQL(CREATE_FOLDER_TABLE);
        db.execSQL(CREATE_RECENT_FILE_TABLE);

    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.e(TAG, "onUpgrade: " + oldVersion + " " + newVersion);
        if (oldVersion < DATABASE_VERSION) {
            dropTables(sqLiteDatabase);
        }
    }

    private void dropTables(SQLiteDatabase sqLiteDatabase) {
        for (String tableName : ALL_TABLES) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + tableName + "'");
        }
        onCreate(sqLiteDatabase);
    }

    @Override
    public synchronized void close() {
        super.close();
    }


    // Insert a folder info into the database
    public void addFolder(String name, String color, int icon) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(Constants.FOLDER_NAME, name);
            values.put(Constants.FOLDER_COLOR, color);
            values.put(Constants.FOLDER_ICON, icon);
            db.insert(TABLE_FOLDERS, null, values);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addRecentFile(String filePath) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.FILE_PATH, filePath);
        db.insert(TABLE_RECENT, null, values);
        db.close();

    }

    public List<String> getAllRecentFile() {

        String selectQuery = "SELECT  * FROM " + TABLE_RECENT;

        Cursor cursor = getWritableDatabase().rawQuery(selectQuery, null);
        List<String> fileList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String file = cursor.getString(1);
                fileList.add(file);
            } while (cursor.moveToNext());
        }
        cursor.close();


        return fileList;
    }

    public boolean deleteRecentFile(String name) {

        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_RECENT, Constants.FILE_PATH + "=" + "'" + name + "'", null) > 0;
    }


}
