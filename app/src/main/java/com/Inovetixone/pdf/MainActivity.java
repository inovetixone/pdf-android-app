package com.Inovetixone.pdf;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.navigation.Navigation;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.Inovetixone.pdf.databinding.ActivityMainBinding;
import com.Inovetixone.pdf.scan.AppScanActivity;
import com.Inovetixone.pdf.ui.pdf_view.PdfViewActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    private BottomSheetDialog mBottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        HandleExternalData();


        binding.addBtn.setOnClickListener(v -> showBottomSheetDialog());

        binding.homeBtn.setOnClickListener(v -> {
            Navigation.findNavController(findViewById(R.id.nav_host_fragment)).navigate(R.id.homeFragment);
            binding.homeBtn.setColorFilter(getResources().getColor(R.color.colorPrimary));
            binding.myFiles.setColorFilter(getResources().getColor(R.color.black));

        });

        binding.myFiles.setOnClickListener(v -> {
            Navigation.findNavController(findViewById(R.id.nav_host_fragment)).navigate(R.id.myFilesFragment);
            binding.homeBtn.setColorFilter(getResources().getColor(R.color.black));
            binding.myFiles.setColorFilter(getResources().getColor(R.color.colorPrimary));
        });


    }





    private void HandleExternalData() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        Uri imageUri = null;
        if ((Intent.ACTION_SEND.equals(action) || Intent.ACTION_VIEW.equals(action)) && type != null) {
            if ("application/pdf".equals(type)) {
                if (Intent.ACTION_SEND.equals(action))
                    imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                else if (Intent.ACTION_VIEW.equals(action))
                    imageUri = intent.getData();
                if (imageUri != null) {
                    ArrayList<Uri> list = new ArrayList<>();
                    list.add(imageUri);
                    StartSignatureActivity("PDFOpen", list);
                }
            }
        }
    }

    public void StartSignatureActivity(String message, ArrayList<Uri> imageUris) {
        Intent intent = new Intent(MainActivity.this, PdfViewActivity.class);
        intent.putExtra("ActivityAction", message);
        intent.putExtra("PDFOpen", imageUris);
        startActivity(intent);
    }

    private void showBottomSheetDialog() {
        final View view = getLayoutInflater().inflate(R.layout.home_bottom, null);


        (view.findViewById(R.id.cameraLayout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppScanActivity.start(MainActivity.this);
            }
        });

        (view.findViewById(R.id.galleryLayout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppScanActivity.start(MainActivity.this);
            }
        });

        (view.findViewById(R.id.browseLayout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppScanActivity.start(MainActivity.this);
            }
        });





        mBottomSheetDialog = new BottomSheetDialog(this,R.style.BottomSheetDialog);
        mBottomSheetDialog.setContentView(view);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }


}