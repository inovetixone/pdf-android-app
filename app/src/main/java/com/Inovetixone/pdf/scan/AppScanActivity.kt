package com.Inovetixone.pdf.scan

import android.Manifest
import android.app.Dialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.DIRECTORY_DCIM
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import com.Inovetixone.pdf.MainActivity
import com.Inovetixone.pdf.R
import com.Inovetixone.pdf.databinding.AppScanActivityLayoutBinding
import com.Inovetixone.pdf.scan.adapters.ImageAdapter
import com.Inovetixone.pdf.scan.adapters.ImageAdapterListener
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Image
import com.itextpdf.text.pdf.PdfWriter
import com.tbruyelle.rxpermissions3.RxPermissions
import com.zynksoftware.documentscanner.ScanActivity
import com.zynksoftware.documentscanner.model.DocumentScannerErrorModel
import com.zynksoftware.documentscanner.model.ScannerResults
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class AppScanActivity: ScanActivity(), ImageAdapterListener {

    companion object {
        private val TAG = AppScanActivity::class.simpleName

        @JvmStatic
        fun start(context: Context) {
            val intent = Intent(context, AppScanActivity::class.java)
            context.startActivity(intent)
        }

    }

    private var alertDialogBuilder: android.app.AlertDialog.Builder? = null
    private var alertDialog: android.app.AlertDialog? = null
    lateinit var binding: AppScanActivityLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AppScanActivityLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        addFragmentContentLayout()
    }

    override fun onError(error: DocumentScannerErrorModel) {
        showAlertDialog(getString(R.string.error_label), error.errorMessage?.error, getString(R.string.ok_label))
    }

    override fun onSuccess(scannerResults: ScannerResults) {
        initViewPager(scannerResults)
    }

    override fun onClose() {
        Log.d(TAG, "onClose")
        finish()
    }

    override fun onSaveButtonClicked(image: File) {
        checkForStoragePermissions(image)
    }

    private fun checkForStoragePermissions(image: File) {
        RxPermissions(this)
            .requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
            .subscribe { permission ->
                when {
                    permission.granted -> {
                        saveImage(image)
                    }
                    permission.shouldShowRequestPermissionRationale -> {
                        onError(DocumentScannerErrorModel(DocumentScannerErrorModel.ErrorMessage.STORAGE_PERMISSION_REFUSED_WITHOUT_NEVER_ASK_AGAIN))
                    }
                    else -> {
                        onError(DocumentScannerErrorModel(DocumentScannerErrorModel.ErrorMessage.STORAGE_PERMISSION_REFUSED_GO_TO_SETTINGS))
                    }
                }
            }
    }

    private fun saveImage(image: File) {
        showProgressBar()

        val date = Date()
        val formatter = SimpleDateFormat("dd_MM_yyyy_HH_mm_ss:mm", Locale.getDefault())
        val dateFormatted = formatter.format(date)

        val to = File(Environment.getExternalStorageDirectory().absolutePath + "/" + DIRECTORY_DCIM + "/zynkphoto${dateFormatted}.jpg")

        val inputStream: InputStream = FileInputStream(image)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val resolver: ContentResolver = contentResolver
            val contentValues = ContentValues()
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "zynkphoto${dateFormatted}.jpg")
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/*")
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM")
            val imageUri: Uri? = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
            val out = resolver.openOutputStream(imageUri!!)
            out?.write(image.readBytes())
            out?.flush()
            out?.close()
        } else {
            val out: OutputStream = FileOutputStream(to)

            val buf = ByteArray(1024)
            var len: Int
            while (inputStream.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }
            inputStream.close()
            out.flush()
            out.close()
        }

        hideProgessBar()
        savePDFDocument(image)
        //showAlertDialog(getString(R.string.photo_saved), "", getString(R.string.ok_label))
    }

    private fun showProgressBar() {
        binding.progressLayoutApp.isVisible = true
    }

    private fun hideProgessBar() {
        binding.progressLayoutApp.isVisible = false
    }

    private fun initViewPager(scannerResults: ScannerResults) {
        val fileList = ArrayList<File>()

        scannerResults.originalImageFile?.let {
            Log.d(TAG, "ZDCoriginalPhotoFile size ${it.sizeInMb}")
        }

        scannerResults.croppedImageFile?.let {
            Log.d(TAG, "ZDCcroppedPhotoFile size ${it.sizeInMb}")
        }

        scannerResults.transformedImageFile?.let {
            Log.d(TAG, "ZDCtransformedPhotoFile size ${it.sizeInMb}")
        }

        scannerResults.originalImageFile?.let { fileList.add(it) }
        scannerResults.transformedImageFile?.let { fileList.add(it) }
        scannerResults.croppedImageFile?.let { fileList.add(it) }
        val targetAdapter = ImageAdapter(this, fileList, this)
        binding.viewPagerTwo.adapter = targetAdapter
        binding.viewPagerTwo.isUserInputEnabled = false

        binding.previousButton.setOnClickListener {
            binding.viewPagerTwo.currentItem = binding.viewPagerTwo.currentItem - 1
            binding.nextButton.isVisible = true
            if (binding.viewPagerTwo.currentItem == 0) {
                binding.previousButton.isVisible = false
            }
        }

        binding.nextButton.setOnClickListener {
            binding.viewPagerTwo.currentItem = binding.viewPagerTwo.currentItem + 1
            binding.previousButton.isVisible = true
            if (binding.viewPagerTwo.currentItem == fileList.size - 1) {
                binding.nextButton.isVisible = false
            }
        }
    }

    private fun showAlertDialog(title: String?, message: String?, buttonMessage: String) {
        alertDialogBuilder = android.app.AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(buttonMessage) { dialog, which ->

            }
        alertDialog?.dismiss()
        alertDialog = alertDialogBuilder?.create()
        alertDialog?.setCanceledOnTouchOutside(false)
        alertDialog?.show()
    }

    val File.size get() = if (!exists()) 0.0 else length().toDouble()
    val File.sizeInKb get() = size / 1024
    val File.sizeInMb get() = sizeInKb / 1024


    @Throws(DocumentException::class, IOException::class)
    fun createPdf(fileName: String,img: File) {
        try {
            val document = Document()
            val directoryPath =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                    .toString() + "/" + getString(R.string.app_name) + "/MyPDF/"
            PdfWriter.getInstance(
                document,
                FileOutputStream("$directoryPath$fileName.pdf")
            ) //  Change pdf's name.
            document.open()
            val image: Image = Image.getInstance(img.path) // Change image's name and extension.
            val scaler = (document.pageSize.width - document.leftMargin()
                    - document.rightMargin() - 0) / image.width * 100 // 0 means you have no indentation. If you have any, change it.
            image.scalePercent(scaler)
            image.alignment = Image.ALIGN_CENTER or Image.ALIGN_TOP
            image.setAbsolutePosition(
                (document.pageSize.width - image.scaledWidth) / 2.0f,
                (document.pageSize.height - image.scaledHeight) / 2.0f
            )
            document.add(image)
            document.close()
            Toast.makeText(AppScanActivity@this, "Successfully PDF Created", Toast.LENGTH_SHORT).show()
            startActivity(Intent(AppScanActivity@this, MainActivity::class.java))
        } catch (e: Exception) {
        }
    }

    fun savePDFDocument(image: File) {
        val dialog = Dialog(AppScanActivity@this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val alertView = layoutInflater.inflate(R.layout.file_alert_dialog, null)
        val edittext = alertView.findViewById<EditText>(R.id.editText2)
        dialog.setContentView(alertView)
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window!!.attributes = lp
        dialog.findViewById<View>(R.id.bt_close).setOnClickListener { dialog.dismiss() }
        dialog.findViewById<View>(R.id.bt_save).setOnClickListener {
            val fileName = edittext.text.toString()
            if (fileName.length == 0) {
                Toast.makeText(AppScanActivity@this, "File name should not be empty", Toast.LENGTH_LONG)
                    .show()
            } else {
                try {
                    createPdf(fileName,image)
                } catch (e: DocumentException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                dialog.dismiss()
            }
        }
    }








}