package com.Inovetixone.pdf.scan.adapters

import java.io.File

interface ImageAdapterListener {
    fun onSaveButtonClicked(image: File)
}